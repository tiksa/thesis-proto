#!/bin/bash

echo "Killing containers"
docker kill $(docker ps -a -q)
echo "Removing containers"
docker rm $(docker ps -a -q)

echo "Initializing database volume"
rm -rf /var/transcoder-mysql/*

( cd thesis-fileserver && ./docker-run.sh )
( cd thesis-mysql && ./docker-run.sh )
( cd thesis-webapp && ./docker-run.sh )
( cd thesis-worker && ./docker-run.sh )