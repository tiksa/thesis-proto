#!/bin/bash

sudo docker build -t tiksa/thesis-fileserver .

sudo docker kill fileserver
sudo docker rm fileserver

sudo docker run --name fileserver \
	-p 3001:3000 \
	-d tiksa/thesis-fileserver
