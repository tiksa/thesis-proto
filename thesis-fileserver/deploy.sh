#!/bin/bash

ip=52.16.214.93

if [ "$1" == "dev" ]; then
	service="thesis-fileserver-dev"
else
	service="thesis-fileserver"
fi

fleet_cmd="sudo mkdir -p /var/thesis-fileserver/files/ && while [ \$(fleetctl list-units | grep fileserver | wc -l) -gt 0 ]; \
	do fleetctl destroy \$(fleetctl list-unit-files | grep fileserver | head -1 | cut -f 1); done &&\
	cd thesis-fileserver && fleetctl submit $service.service && \
	fleetctl start $service"

echo "* Deploying $service"
echo "* Sending service files"
ssh -i ~/.ssh/thesis.pem core@$ip mkdir -p thesis-fileserver
scp -i ~/.ssh/thesis.pem -r *.service core@$ip:~/thesis-fileserver

if [ "$1" == "dev" ]; then
	echo "* Sending source files"
	scp -i ~/.ssh/thesis.pem -r *.js package.json core@$ip:~/thesis-fileserver
fi

echo "* Starting container via fleet"
ssh -i ~/.ssh/thesis.pem core@$ip $fleet_cmd