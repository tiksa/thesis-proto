var static = require('node-static'),
	mkdirp = require('mkdirp'),
	express = require('express'),
	mv = require('mv'),
	Etcd = require('node-etcd'),
	pkgjson = require('./package.json');

var app = express();

var UPLOAD_DIR = "/files/",
	PORT = 3000;

app.use(express.logger());
app.use(express.bodyParser({
	keepExtensions: true,
	uploadDir: UPLOAD_DIR
}));
app.use(express.static(UPLOAD_DIR));

app.post('/', function(req, res) {
	var outputFilename = req.body.outputFilename;
	var tmpPath = req.files.file.path;
	var targetPath = UPLOAD_DIR + outputFilename;
	console.log("Moving from " + tmpPath + " to " + targetPath);

	mv(tmpPath, targetPath, function(err) {
		if (err)
			console.log(err);
	});

	res.end();
});


mkdirp.sync(UPLOAD_DIR);

var etcd = new Etcd(['127.0.0.1:4001', '172.31.28.237:4001', '172.31.31.154:4001']);

function etcdRegister() {
	etcd.set(pkgjson.name, JSON.stringify({
		hostname: process.env.COREOS_PRIVATE_IPV4,
		port: process.env.COREOS_PORT
	}), {
		ttl: 10
	});

	setTimeout(etcdRegister, 5000);
}

require('http').createServer(app).listen(PORT, function() {
	console.log("File server running at port " + PORT);
	etcdRegister();
});
