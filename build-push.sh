#!/bin/bash

docker build -t tiksa/thesis-fileserver thesis-fileserver/
docker push tiksa/thesis-fileserver

docker build -t tiksa/thesis-mysql thesis-mysql/
docker push tiksa/thesis-mysql

docker build -t tiksa/thesis-webapp thesis-webapp/
docker push tiksa/thesis-webapp

docker build -t tiksa/thesis-worker thesis-worker/
docker push tiksa/thesis-worker
