#!/bin/bash

ip=52.16.214.93

scp -i ~/.ssh/thesis.pem thesis-fileserver/*.service core@$ip:~
scp -i ~/.ssh/thesis.pem thesis-mysql/*.service core@$ip:~
scp -i ~/.ssh/thesis.pem thesis-webapp/*.service core@$ip:~
scp -i ~/.ssh/thesis.pem thesis-worker/*.service core@$ip:~