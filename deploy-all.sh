#!/bin/bash

( cd thesis-fileserver && ./deploy.sh $1 )
( cd thesis-mysql && ./deploy.sh $1 )
( cd thesis-webapp && ./deploy.sh $1 )
( cd thesis-worker && ./deploy.sh $1 )