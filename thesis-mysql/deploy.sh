#!/bin/bash

ip=52.16.214.93
service="thesis-mysql"

fleet_cmd="while [ \$(fleetctl list-units | grep mysql | wc -l) -gt 0 ]; \
	do fleetctl destroy \$(fleetctl list-unit-files | grep mysql | head -1 | cut -f 1); done &&\
	cd thesis-mysql && fleetctl submit $service.service && \
	fleetctl start $service"

echo "* Deploying $service"
echo "* Starting container via fleet"
ssh -i ~/.ssh/thesis.pem core@$ip mkdir -p thesis-mysql
scp -i ~/.ssh/thesis.pem -r *.service core@$ip:~/thesis-mysql
ssh -i ~/.ssh/thesis.pem core@$ip $fleet_cmd