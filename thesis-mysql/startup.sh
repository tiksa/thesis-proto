#!/bin/bash

if [ ! -f /var/lib/mysql/ibdata1 ]; then

    mysql_install_db

    /usr/bin/mysqld_safe &
    sleep 8s

    mysql -uroot < init-db.sql

    killall mysqld
    sleep 8s
fi

/usr/bin/mysqld_safe