#!/bin/bash

sudo docker build -t tiksa/thesis-mysql .

sudo docker kill mysql
sudo docker rm mysql

sudo docker run --name mysql \
	-v /var/transcoder-mysql/:/var/lib/mysql \
	-d tiksa/thesis-mysql
