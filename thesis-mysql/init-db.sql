CREATE DATABASE IF NOT EXISTS transcoder;

GRANT ALL PRIVILEGES ON transcoder.* TO 'tiksa'@'%' IDENTIFIED BY 'password';

CREATE TABLE IF NOT EXISTS transcoder.job (
	id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	title VARCHAR(50) DEFAULT NULL,
	file_id VARCHAR(8) DEFAULT NULL,
	created DATETIME DEFAULT NULL,
	aspect_ratio DECIMAL(6,5) DEFAULT NULL,
	quality ENUM('qvga', 'vga', 'svga') DEFAULT NULL,
	status ENUM('unprocessed', 'processing', 'processed')
) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8;