var mysql = require('mysql');

var conn = mysql.createConnection({
	host: process.env.MYSQL_PORT_3306_TCP_ADDR || 'localhost',
	user: process.env.MYSQL_ENV_MYSQL_USER || 'tiksa',
	password: process.env.MYSQL_ENV_MYSQL_PASSWORD || 'password',
	database: process.env.MYSQL_ENV_MYSQL_DATABASE || 'transcoder'
});

exports.conn = conn;