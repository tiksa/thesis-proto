#!/bin/bash

ip=52.16.214.93

if [ "$1" == "dev" ]; then
	service="thesis-webapp-dev"
else
	service="thesis-webapp"
fi

fleet_cmd="while [ \$(fleetctl list-units | grep webapp | wc -l) -gt 0 ]; \
	do fleetctl destroy \$(fleetctl list-unit-files | grep webapp | head -1 | cut -f 1); done &&\
	cd thesis-webapp && fleetctl submit $service.service && \
	fleetctl start $service"

echo "* Deploying $service"
echo "* Sending service files"
ssh -i ~/.ssh/thesis.pem core@$ip mkdir -p thesis-webapp
scp -i ~/.ssh/thesis.pem -r *.service core@$ip:~/thesis-webapp

if [ "$1" == "dev" ]; then
	echo "* Sending source files"
	scp -i ~/.ssh/thesis.pem -r *.js package.json routes/ services/ views/ core@$ip:~/thesis-webapp
fi

echo "* Starting container via fleet"
ssh -i ~/.ssh/thesis.pem core@$ip $fleet_cmd