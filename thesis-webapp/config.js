module.exports = {
	PORT: 3000,
	HOST_FILESERVER: process.env.FILESERVER_PORT.replace("tcp", "http")
};