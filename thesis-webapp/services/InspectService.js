var _ = require('underscore'),
	Promise = require('bluebird'),
	mediainfo = require('mediainfo');

function getMediainfo(file) {
	return new Promise(function (resolve, reject) {
		mediainfo(file, function (err, mis) {
			if (err)
				reject(err);

			resolve(_.first(mis));
		});
	});
};

function qualitiesFromMediainfo(mi) {
	var h = parseHeight(mi);
	var qualities = [{name: "qvga", height: 240}, {name: "vga", height: 480}, {name: "svga", height: 600}];

	var profiles = _.chain(qualities)
	.filter(function (quality) {
		return h >= quality.height; 
	})
	.pluck('name')
	.value();

	return profiles;
}

function parseWidth(mi) {
	var videoTrack = _.findWhere(mi.tracks, { type: "Video" });
	var str = videoTrack.width;
	var width = parseDimension(str);

	return width;
}

function parseHeight(mi) {
	var videoTrack = _.findWhere(mi.tracks, { type: "Video" });
	var str = videoTrack.height;
	var height = parseDimension(str);

	return height;
}

function parseDimension(str) {
	return parseInt(str.substring(0, str.length - " pixels".length).replace(' ', ''));
}

function parseAspectRatio(mi) {
	return parseWidth(mi) / parseHeight(mi);
}

exports.getJobs = function (file, fileId, originalFilename) {
	return new Promise(function (resolve, reject) {
		getMediainfo(file)
		.then(function (mi) {
			var qualities = qualitiesFromMediainfo(mi);

			var aspectRatio = parseAspectRatio(mi);

			var jobs = [];
			_.each(qualities, function (quality) {
				jobs.push({
					title: originalFilename,
					created: new Date(),
					fileId: fileId,
					aspectRatio: aspectRatio,
					quality: quality,
					status: "unprocessed"
				});
			});

			resolve(jobs);
		})
		.catch(function (err) {
			reject(err);
		});
	});
};
