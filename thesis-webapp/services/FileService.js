var	Promise = require('bluebird'),
	fs = require('fs'),
	FormData = require('form-data'),
	config = require('../config');

exports.sendToFileServer = function (file, outputFilename) {
	return new Promise(function (resolve, reject) {
		
		var form = new FormData();
		form.append('file', fs.createReadStream(file));
		form.append('outputFilename', outputFilename);
		form.submit(config.HOST_FILESERVER, function (err, res) {
			if (err)
				return reject(err);

			resolve("OK");
		});
	});
};
