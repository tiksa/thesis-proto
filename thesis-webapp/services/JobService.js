var conn = require('../db').conn,
	_ = require('underscore'),
	async = require('async'),
	Promise = require('bluebird'),
	fs = require('fs');

exports.addJobs = function (jobs) {
	return new Promise(function (resolve, reject) {
		async.eachSeries(jobs, addJob, function (err) {
			if (err)
				reject(err);

			resolve("OK");
		});
	});
};

function addJob(job, cb) {
	conn.query('INSERT INTO job SET ?', [sqlize(job)], function (err, result) {
		if (err)
			cb(err);

		cb();
	});
}

exports.chooseJob = function () {
	return new Promise(function (resolve, reject) {
		conn.beginTransaction(function (err) {
			if (err)
				return reject(err);

			conn.query('SELECT * FROM job WHERE status="unprocessed"', function(err, jobs, fields) {
				if (err) {
					conn.rollback(function () {
						return reject(err);
					});
				}

				if (_.isEmpty(jobs))
					return resolve({});

				var chosen = _.first(jobs);

				conn.query('UPDATE job SET status="processing" WHERE id=?', [chosen.id], function (err, result) {
					if (err) {
						conn.rollback(function () {
							return reject(err);
						});
					}

					conn.commit(function (err) {
						if (err) {
							conn.rollback(function () {
								return reject(err);
							});
						}

						chosen.status = "processing";
						resolve(unSqlize(chosen));
					});
				});
			});
		});
	});
};

exports.markJobProcessed = function (jobId) {
	return new Promise(function (resolve, reject) {
		conn.query('UPDATE job SET status="processed" WHERE id=?', [jobId], function (err, result) {
			if (err)
				reject(err);

			resolve("OK");
		});
	});
};

exports.getAvailableJobs = function() {
	return new Promise(function (resolve, reject) {
		conn.query('SELECT * FROM job WHERE status="unprocessed"', function(err, jobs, fields) {
			if (err)
				return reject(err);

			jobs = _.map(jobs, function (job) {
				return unSqlize(job);
			});

			resolve(jobs);
		});
	});
};

exports.getAllJobs = function() {
	return new Promise(function (resolve, reject) {
		conn.query('SELECT * FROM job', function(err, jobs, fields) {
			if (err)
				return reject(err);

			jobs = _.map(jobs, function (job) {
				return unSqlize(job);
			});

			resolve(jobs);
		});
	});
};

function sqlize(job) {;
	job.file_id = job.fileId;
	job.aspect_ratio = job.aspectRatio;

	delete job.fileId;
	delete job.aspectRatio;
	
	return job;
}

function unSqlize(job) {
	job.fileId = job.file_id;
	job.aspectRatio = job.aspect_ratio;

	delete job.file_id;
	delete job.aspect_ratio;
	
	return job;
}