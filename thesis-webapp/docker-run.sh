#!/bin/bash

sudo docker build -t tiksa/thesis-webapp .

sudo docker kill webapp
sudo docker rm webapp

sudo docker run --name webapp \
	 --link mysql:mysql \
	 --link fileserver:fileserver \
	-p 3000:3000 \
	-d tiksa/thesis-webapp
