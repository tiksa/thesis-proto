var express = require('express');
var path = require('path');
var favicon = require('static-favicon');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var multiparty = require('multiparty');
var mv = require('mv');
var randomstring = require('randomstring');
var _ = require('underscore');
var jobService = require('./services/JobService');
var inspectService = require('./services/InspectService');
var fileService = require('./services/FileService');
var config = require('./config');
var Etcd = require('node-etcd');
var pkgjson = require('./package.json');
var request = require('request');
var httpProxy = require('http-proxy');

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(cookieParser());

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

var argPort = process.argv[2];
var port = argPort || process.env.PORT || config.PORT || 3000;
app.set('port', port);
app.use(favicon());
app.disable('etag');
app.disable('x-powered-by');
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function (req, res, next) {
	jobService.getAllJobs()
	.then(function (jobs) {
		res.render('index', { jobs: jobs });
	})
	.catch(next);
});

app.get('/available-jobs', function (req, res, next) {
	jobService.getAvailableJobs()
	.then(function (result) {
		res.send(result);
	})
	.catch(next);
});

app.get('/choose-job', function (req, res, next) {
	jobService.chooseJob()
	.then(function (result) {
		res.send(result);
	})
	.catch(function (err) {
		next(err);
	});
});

var proxy = new httpProxy.createProxyServer({});
app.get("/files/*", function(req, res) {
	console.log(req.url);
	req.url = req.url.substring(6);
	console.log(req.url);
    return proxy.web(req, res, {
    	target: config.HOST_FILESERVER
    })
});

/*app.get('/files/:file', function (req, res, next) {
	console.log("noiniii", req.params.file, config.HOST_FILESERVER);
	request.get(config.HOST_FILESERVER + '/' + req.params.file)
	.on('response', function (r) {
		console.log("ahieee", r.statusCode);
	})
	.on('error', function (err) {
		next(err);
	})
	.pipe(res);
});*/

app.post('/upload', function (req, res, next) {
	var form = new multiparty.Form();

	form.parse(req, function (err, fields, files) {
		var upload = _.first(files.upload);
		var srcPath = upload.path;
		var fileType = _.last(srcPath.split('.'));
		var fileId = randomstring.generate(8);
		var destFilename = fileId + "." + fileType;

		fileService.sendToFileServer(srcPath, destFilename)
		.then(function () {
			inspectService.getJobs(srcPath, fileId, upload.originalFilename)
			.then(jobService.addJobs)
			.then(function (result) {
				res.redirect('/');
			})
			.catch(next);
		})
		.catch(next);
	});
});

app.post('/job-processed/:jobId', function (req, res, next) {
	var jobId = req.params.jobId;

	jobService.markJobProcessed(jobId)
	.then(function (result) {
		res.send(result);
	})
	.catch(next);
});

app.use(function(req, res, next) {
	var err = new Error('Not Found');
	err.status = 404;
	next(err);
});

if (app.get('env') === 'development') {
	app.use(function(err, req, res, next) {
		console.trace(err);
		res.status(err.status || 500);
		res.render('error', {
			message: err.message,
			error: err
		});
	});
}

// production error handler
app.use(function(err, req, res, next) {
	console.trace(err);
	res.status(err.status || 500);
	res.render('error', {
		message: err.message,
		error: {}
	});
});

app.listen(app.get('port'), function() {
	console.log('Express server listening on port ' + port);
	etcdRegister();
});

var etcd = new Etcd(['127.0.0.1:4001', '172.31.28.237:4001', '172.31.31.154:4001']);

function etcdRegister() {
	etcd.set(pkgjson.name, JSON.stringify({
		hostname: process.env.COREOS_PRIVATE_IPV4,
		port: process.env.COREOS_PORT
	}), {
		ttl: 7
	});

	setTimeout(etcdRegister, 5000);
}