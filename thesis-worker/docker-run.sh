#!/bin/bash

sudo docker build -t tiksa/thesis-worker .

sudo docker kill worker
sudo docker rm worker

sudo docker run --name worker \
	--link webapp:webapp \
	--link fileserver:fileserver \
	-d tiksa/thesis-worker
