var exec = require('./exec'),
	Promise = require('bluebird'),
	fs = require('fs'),
	FormData = require('form-data'),
	worker = require('./worker'),
	mkdirp = require('mkdirp');

mkdirp.sync('./transcoded');

exports.transcode = function (job) {
	return new Promise(function (resolve, reject) {
		var targetPath = outputFile(job.fileId, job.quality);
		var cmd = "ffmpeg -y -i " + inputFile(job.fileId) + " -an -vsync 1 -vcodec libx264 -profile:v " + profile(job.quality) + " -crf 20 -preset veryfast -r 25 -s " + size(job.quality, job.aspectRatio) + " " + targetPath;

		exec.exec({ cmd: cmd }, function (err, result) {
			if (err)
				return reject(err);

			sendToFileServer(targetPath, job.fileId + "-" + job.quality + ".mp4")
			.then(function (res) {
				fs.unlinkSync(targetPath);
				return resolve(res);
			})
			.catch(function (err) {
				return reject(err);
			});
		});
	});
};

function profile(quality) {
	return {
		"qvga": "baseline",
		"vga": "main",
		"svga": "high"
	}[quality];
}

function inputFile(fileId) {
	return worker.getHost('fileserver') + "/" + fileId + ".mp4";
}

function outputFile(fileId, quality) {
	return "./transcoded/" + fileId + "-" + quality + ".mp4";
}

function size(quality, aspectRatio) {
	return {
		"qvga": "320x240",
		"vga": "600x480",
		"svga": "800x600"
	}[quality];

	/*var h =  {
		"qvga": 240,
		"vga": 480,
		"svga": 600
	}[quality];
	var w = Math.round(aspectRatio * h);
	if (w % 2 !== 0)
		w++;

	return w + "x" + h;*/
}

function sendToFileServer(file, outputFilename) {
	return new Promise(function (resolve, reject) {
		
		var form = new FormData();
		form.append('file', fs.createReadStream(file));
		form.append('outputFilename', outputFilename);
		form.submit(worker.getHost('fileserver'), function (err, res) {
			if (err)
				return reject(err);

			resolve("OK");
		});
	});
};