var request = require('request'),
	transcoder = require('./transcoder'),
	_ = require('underscore'),
	Etcd = require('node-etcd');

var polling = false,
	jobTaken = false,
	hosts = {};

function poll() {
	if (polling)
		return;

	polling = true;
	request(exports.getHost('webapp') + "/choose-job", function (err, res, body) {
		polling = false;
		if (err || res.statusCode !== 200) {
			console.log(err);
			return;
		}
		var job = JSON.parse(body);
		if (_.isEmpty(job)) {
			return;
		}
		jobTaken = true;
		console.log("Found job: ", job);

		transcoder.transcode(job)
		.then(function (res) {
			console.log("Job processed: ", job);
			request.post(exports.getHost('webapp') + "/job-processed/" + job.id, {}, function (err, res, body) {
				jobTaken = false;
				
				if (err || res.statusCode !== 200) {
					console.log(err);
					return;
				}
			});
		})
		.catch(function (err) {
			console.log(err);
			jobTaken = false;
		});
	});
}

exports.getHost = function (serviceName) {
	return "http://" + hosts[serviceName].hostname + ":" + hosts[serviceName].port;
};

var etcd = new Etcd(['127.0.0.1:4001', '172.31.30.233:4001', '172.31.31.154:4001']);

etcd.get('thesis-webapp', function (err, value, asd) {
	if (err) {
		console.log(err);
		process.exit();
	}

	hosts.webapp = JSON.parse(value.node.value);
	etcd.get('thesis-fileserver', function (err, value) {
		if (err) {
			console.log(err);
			process.exit();
		}

		hosts.fileserver = JSON.parse(value.node.value);
		setInterval(function () {
			if (!polling && !jobTaken)
				poll();
		}, 500);
	});
});