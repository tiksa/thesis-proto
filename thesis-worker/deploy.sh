#!/bin/bash

ip=52.16.214.93

if [ "$1" == "dev" ]; then
	service="thesis-worker-dev@"
else
	service="thesis-worker@"
fi

if [ "$1" == "dev" ]; then
	fleet_cmd="while [ \$(fleetctl list-units | grep worker | wc -l) -gt 0 ]; \
	do fleetctl destroy \$(fleetctl list-unit-files | grep worker | head -1 | cut -f 1); done &&\
	cd thesis-worker && fleetctl submit $service.service && \
	fleetctl start ${service}1"
else
	fleet_cmd="while [ \$(fleetctl list-units | grep worker | wc -l) -gt 0 ]; \
	do fleetctl destroy \$(fleetctl list-unit-files | grep worker | head -1 | cut -f 1); done &&\
	cd thesis-worker && fleetctl submit $service.service && \
	fleetctl start ${service}1 && \
	fleetctl start ${service}2 && \
	fleetctl start ${service}3 && \
	fleetctl start ${service}4 && \
	fleetctl start ${service}5 && \
	fleetctl start ${service}6"
fi

echo "* Deploying $service"
echo "* Sending service files"
ssh -i ~/.ssh/thesis.pem core@$ip mkdir -p thesis-fileserver
scp -i ~/.ssh/thesis.pem *.service core@$ip:~/thesis-worker


if [ "$1" == "dev" ]; then
	echo "* Sending source files"
	scp -i ~/.ssh/thesis.pem -r *.js package.json core@$ip:~/thesis-worker
fi

echo "* Starting container via fleet"
ssh -i ~/.ssh/thesis.pem core@$ip $fleet_cmd