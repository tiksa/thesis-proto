var spawn = require('child_process').spawn,
	mkdirp = require('mkdirp'),
	fs = require('fs');

exports.exec = function(options, callback) {
	var cmd = options.cmd,
		outputFile = options.outputFile,
		dataCallback = options.dataCallback;

	console.log("Executing command: " + cmd);
	var parts = cmd.split(" ");
	var process = spawn(parts[0], parts.slice(1));

	var output = "";

	process.stdout.on('data', function(data) {
		if (dataCallback)
			dataCallback(data);
		output += data;
	});

	process.stderr.on('data', function(data) {
		if (dataCallback)
			dataCallback(data);
		output += data;
	});

	process.on('exit', function(code) {
		console.log("Process exited with code " + code);
		writeToFile(outputFile, output);
		var err;
		if (code !== 0)
			err = new Error("Exit code: " + code);

		callback(err, {
			code: code,
			output: output
		});
	});
};

function writeToFile(file, data) {
	if (!file) {
		return;
	}

	fs.writeFile(file, data, function(err) {
		if (err) {
			var dirPath = __dirname + "/" + file.substring(0, file.lastIndexOf('/'));
			mkdirp(dirPath, function(err) {
				if (err) {
					throw err;
				}
				var filePath = __dirname + "/" + file;
				fs.writeFile(filePath, data, function(err) {
					if (err) {
						throw err;
					}
				});
			});
		}
	});
}
